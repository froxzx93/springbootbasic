package com.example.service;

import com.example.dto.DTOClienteInput;
import com.example.dto.DTOResponse;

public interface IClienteService {
	public DTOResponse registrar(DTOClienteInput input);

	public DTOResponse obtenerLlave(String rut);

	public DTOResponse obtenerColeccion(String cache);

	public DTOResponse actualizarLlave(DTOClienteInput input);

	public DTOResponse eliminarLlave(String rut);
}
