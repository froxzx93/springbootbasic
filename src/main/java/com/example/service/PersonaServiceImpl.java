package com.example.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.dto.DTOClienteInput;
import com.example.dto.DTOResponse;
import com.example.repository.PersonaRepositoryImpl;

@Service("ClientePersona")
public class PersonaServiceImpl implements IClienteService {

	@Autowired
	PersonaRepositoryImpl personaImpl;
	private static final Logger LOGGER = LoggerFactory.getLogger("web");

	@Override
	public DTOResponse registrar(DTOClienteInput input) {
		LOGGER.info("registrar {}", input.getRut());
		return personaImpl.registrar(input);
	}

	@Override
	public DTOResponse obtenerLlave(String rut) {
		LOGGER.info("obtenerLlave {}", rut);
		return personaImpl.obtenerLlave(rut);
	}

	@Override
	public DTOResponse obtenerColeccion(String cacheName) {
		LOGGER.info("obtenerColeccion {}", cacheName);
		return personaImpl.obtenerColeccion(cacheName);
	}

	@Override
	public DTOResponse actualizarLlave(DTOClienteInput input) {
		LOGGER.info("actualizarLlave {}", input.getRut());
		return personaImpl.actualizarLlave(input);

	}

	@Override
	public DTOResponse eliminarLlave(String rut) {
		LOGGER.info("eliminarLlave {}", rut);
		return personaImpl.eliminarLlave(rut);

	}

}
