package com.example.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.tomcat.util.json.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.cglib.core.CollectionUtils;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import com.example.dto.DTOClienteInput;
import com.example.dto.DTOClienteOut;
import com.example.dto.DTOPersonaOut;
import com.example.dto.DTORedis;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.google.gson.Gson;

import redis.clients.jedis.Jedis;

@Repository
public class RedisRepository implements IRedisRepository {

	private static final String CACHE_NAME = "Cliente::";
	private RedisTemplate<String, DTOClienteInput> redisTemplate;
	private HashOperations<String, String, Object> hashOperations;
	private static final Logger LOGGER = LoggerFactory.getLogger("web");
	@Autowired
	private DTORedis dtoRedis;

	@Autowired
	public RedisRepository(RedisTemplate<String, DTOClienteInput> redisTemplate) {
		LOGGER.info("RedisRepository redisTemplate {}", redisTemplate);
		this.redisTemplate = redisTemplate;
	}

	@PostConstruct
	private void init() {
		LOGGER.info("RedisRepository init");
		hashOperations = redisTemplate.opsForHash();
	}

	@Override
	public DTOPersonaOut findById(String id) {
		DTOPersonaOut response = null;
		String redisResponse;
		try {
			if (dtoRedis.isSerializable()) {
				DTOPersonaOut resp = (DTOPersonaOut) hashOperations.get(CACHE_NAME, id);
				response = resp;
			} else {
				Jedis client = new Jedis(dtoRedis.getServer());
				redisResponse = client.get(CACHE_NAME + id);
				Gson gson = new Gson();
				DTOPersonaOut dataResponse = gson.fromJson(redisResponse, DTOPersonaOut.class);
				response = dataResponse;
			}
		} catch (Exception e) {
			LOGGER.error("RedisRepository findById {}", e);
		}

		return response;
	}

	@Override
	public boolean save(DTOClienteInput input) {
		boolean resp = false;
		DTOPersonaOut persona = null;
		try {
			// Validar que llave no exista
			persona = findById(input.getRut());
			if (persona == null) {
				resp = redisSave(input);
			}
		} catch (Exception e) {
			LOGGER.error("RedisRepository save {}", e);
		}
		return resp;
	}

	@Override
	public boolean put(DTOClienteInput input) {
		boolean resp = false;
		try {
			resp = redisSave(input);
		} catch (Exception e) {
			LOGGER.error("RedisRepository save {}", e);
		}
		return resp;
	}

	@Override
	public boolean delete(String id) {
		boolean resp = false;
		try {
			if (dtoRedis.isSerializable()) {
				hashOperations.delete(CACHE_NAME, id);
			} else {
				Jedis client = new Jedis(dtoRedis.getServer());
				client.del(CACHE_NAME + id);
			}
			resp = true;
		} catch (Exception e) {
			LOGGER.info("Error in delete {}", e);
		}
		return resp;
	}

	@Override
	public List<DTOPersonaOut> findAll(String keys) {
		List<Object> list = new ArrayList<>();
		Gson gson = new Gson();
		List<DTOPersonaOut> listaPersonas = new ArrayList<>();
		if (dtoRedis.isSerializable()) {
			hashOperations.entries(keys);
		} else {
			Jedis client = new Jedis(dtoRedis.getServer());
			Set<String> sets = client.keys("Cliente" + "*");
			for (String set : sets) {
				String personaString = client.get(set);
				LOGGER.info("personaString {}", personaString);
				DTOPersonaOut personaJson = gson.fromJson(personaString, DTOPersonaOut.class);
				listaPersonas.add(personaJson);
				LOGGER.info("persona {}", personaJson);
			}
		}

		return listaPersonas;
	}

	@Override
	public String parseDataRedis(Object obc) {
		ObjectMapper mapper = new ObjectMapper();
		String json = null;
		try {
			json = mapper.writeValueAsString(obc);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return json;
	}

	public boolean redisSave(DTOClienteInput input) {
		boolean response = false;
		try {
			if (dtoRedis.isSerializable()) {
				hashOperations.put(input.getRut(), CACHE_NAME, input);
			} else {
				Jedis client = new Jedis(dtoRedis.getServer());
				String data = parseDataRedis(input);
				client.set(CACHE_NAME + input.getRut(), data);
			}
			response = true;
		} catch (Exception e) {
			LOGGER.error("redisSave {}", e);
		}
		return response;
	}

}
