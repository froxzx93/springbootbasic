package com.example.repository;

import com.example.dto.DTOResponse;

public interface IEmpresaRepository {

	DTOResponse registrar(String nombre);
}
