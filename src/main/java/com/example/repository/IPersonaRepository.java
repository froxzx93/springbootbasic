package com.example.repository;

import com.example.dto.DTOClienteInput;
import com.example.dto.DTOResponse;

public interface IPersonaRepository {

	DTOResponse registrar(DTOClienteInput input);

	DTOResponse obtenerLlave(String rut);

	DTOResponse obtenerColeccion(String cacheName);

	DTOResponse actualizarLlave(DTOClienteInput input);

	DTOResponse eliminarLlave(String rut);
}
