package com.example.repository;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.dto.DTOClienteInput;
import com.example.dto.DTOPersonaOut;
import com.example.dto.DTOResponse;

@Repository
public class PersonaRepositoryImpl implements IPersonaRepository {
	@Autowired
	RedisRepository redis;
	private static Logger LOGGER = LoggerFactory.getLogger(PersonaRepositoryImpl.class);

	@Override
	public DTOResponse registrar(DTOClienteInput input) {
		DTOResponse response = new DTOResponse();
		DTOPersonaOut persona = new DTOPersonaOut();
		boolean isRedis = false;
		try {
			persona.setNombre(input.getNombre());
			persona.setTipoCliente(input.getTipoCliente());
			persona.setRut(input.getRut());
			isRedis = redis.save(input);
			if (isRedis) {
				response.setMessage("Success");
				response.setStatus(200);
				response.setData(persona);
			} else
				throw new Exception();
		} catch (Exception e) {
			response.setStatus(400);
			response.setMessage("Error, validar que no exista registro con ese rut");
			response.setData(null);
			// TODO: handle exception
		}
		return response;
	}

	@Override
	public DTOResponse obtenerLlave(String rut) {
		DTOResponse response = new DTOResponse();
		try {
			response.setMessage("Success");
			response.setStatus(200);
			response.setData(redis.findById(rut));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return response;
	}

	@Override
	public DTOResponse obtenerColeccion(String cacheName) {
		DTOResponse response = new DTOResponse();
		List<DTOPersonaOut> lista = null;
		try {
			lista = redis.findAll(cacheName);
			response.setMessage("Success");
			response.setStatus(200);
			response.setData(lista);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return response;
	}

	@Override
	public DTOResponse actualizarLlave(DTOClienteInput input) {
		DTOResponse response = new DTOResponse();
		DTOPersonaOut persona = new DTOPersonaOut();
		try {
			persona.setNombre(input.getNombre());
			persona.setTipoCliente("Persona");
			persona.setRut(input.getRut());
			redis.put(input);
			response.setMessage("Success");
			response.setStatus(200);
			response.setData(persona);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return response;
	}

	@Override
	public DTOResponse eliminarLlave(String rut) {
		DTOResponse response = new DTOResponse();
		try {
			redis.delete(rut);
			response.setMessage("Success");
			response.setStatus(200);
			response.setData("Delete success " + rut);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return response;
	}

}
