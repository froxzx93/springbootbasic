package com.example.repository;

import java.util.List;
import java.util.Map;

import com.example.dto.DTOClienteInput;
import com.example.dto.DTOPersonaOut;

public interface IRedisRepository {

	List<DTOPersonaOut> findAll(String keys);

	Object findById(String id);

	boolean save(DTOClienteInput input);

	boolean delete(String id);

	String parseDataRedis(Object obc);

	boolean put(DTOClienteInput input);

}
