package com.example.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.example.dto.DTOClienteInput;
import com.example.dto.DTOResponse;
import com.example.factory.FactoryControllerCliente;
import com.example.repository.PersonaRepositoryImpl;
import com.example.service.IClienteService;

@RestController
public class ExampleController {

	@Autowired
	PersonaRepositoryImpl personaR;
	@Autowired
	FactoryControllerCliente factory;
	private static final Logger LOGGER = LoggerFactory.getLogger("web");

	@PostMapping("/api/v1/cliente")
	public DTOResponse addCliente(@RequestBody DTOClienteInput input) {
		IClienteService delegate;
		DTOResponse response;
		delegate = factory.factoryCliente(input);
		response = delegate.registrar(input);
		return response;
	}

	@GetMapping("/api/v1/cliente/{rut}")
	public DTOResponse getCliente(@PathVariable String rut) {
		DTOClienteInput input = new DTOClienteInput();
		input.setTipoCliente("Persona");
		IClienteService delegate;
		DTOResponse response;
		delegate = factory.factoryCliente(input);
		response = delegate.obtenerLlave(rut);
		return response;
	}

	@PutMapping("/api/v1/cliente/")
	public DTOResponse updateCliente(@RequestBody DTOClienteInput input) {
		IClienteService delegate;
		DTOResponse response;
		delegate = factory.factoryCliente(input);
		response = delegate.actualizarLlave(input);
		return response;
	}

	@DeleteMapping("/api/v1/cliente/{rut}")
	public DTOResponse delCliente(@PathVariable String rut) {
		IClienteService delegate;
		DTOClienteInput input = new DTOClienteInput();
		input.setTipoCliente("Persona");
		DTOResponse response;
		delegate = factory.factoryCliente(input);
		response = delegate.eliminarLlave(rut);
		return response;
	}

	@GetMapping("/api/v1/clientes")
	public DTOResponse getClientes() {
		DTOClienteInput input = new DTOClienteInput();
		input.setTipoCliente("Persona");
		IClienteService delegate;
		DTOResponse response;
		delegate = factory.factoryCliente(input);
		response = delegate.obtenerColeccion(input.getCache());
		return response;
	}
}
