package com.example.factory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.example.dto.DTOClienteInput;
import com.example.service.IClienteService;

@Service
public class FactoryControllerCliente {

	@Autowired
	private ApplicationContext apContext;
	private static String DEFAULT = "Persona";
	private static String VAR_CLIENTE = "Cliente";
	private static final Logger LOGGER = LoggerFactory.getLogger("web");

	public IClienteService factoryCliente(DTOClienteInput input) {
		IClienteService client = null;
		try {
			// ClientePersona || ClienteEmpresa
			client = (IClienteService) apContext.getBean(VAR_CLIENTE + input.getTipoCliente());
		} catch (BeansException e) {
			try {
				client = (IClienteService) apContext.getBean(VAR_CLIENTE + DEFAULT);
			} catch (Exception e2) {
				LOGGER.error("IClienteService Exception2 {}", e2);
			}
		} 
		return client;
	}
}
