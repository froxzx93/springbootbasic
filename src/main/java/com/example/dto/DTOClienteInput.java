package com.example.dto;

import java.io.Serializable;
import java.util.Date;
public class DTOClienteInput implements Serializable {

	String cache;
	String rut;
	String tipoCliente;
	String nombre;
	String motivo;
	Date origen;
	boolean internacional;

	public String getCache() {
		return cache;
	}

	public void setCache(String cache) {
		this.cache = cache;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}	

	public String getTipoCliente() {
		return tipoCliente;
	}

	public void setTipoCliente(String tipoCliente) {
		this.tipoCliente = tipoCliente;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public Date getOrigen() {
		return origen;
	}

	public void setOrigen(Date origen) {
		this.origen = origen;
	}

	public boolean isInternacional() {
		return internacional;
	}

	public void setInternacional(boolean internacional) {
		this.internacional = internacional;
	}

	@Override
	public String toString() {
		return "DTOClienteInput [cache=" + cache + ", rut=" + rut + ", tipo=" + tipoCliente + ", nombre=" + nombre
				+ ", motivo=" + motivo + ", origen=" + origen + ", internacional=" + internacional + "]";
	}
	
}
