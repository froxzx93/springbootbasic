package com.example.dto;

import java.time.LocalDate;

public class DTOClienteOut {

	String nombre;
	String rut;
	LocalDate fecha;
	String tipoCliente;
	
	public String getTipoCliente() {
		return tipoCliente;
	}

	public void setTipoCliente(String tipoCliente) {
		this.tipoCliente = tipoCliente;
	}

	public LocalDate getFecha() {
		return LocalDate.now();
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

}
