package com.example.dto;

import java.util.Date;

public class DTOEmpresaOut extends DTOClienteOut {

	String motivo;
	Date origen;
	boolean internacional;
	
	public String getMotivo() {
		return motivo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	public Date getOrigen() {
		return origen;
	}
	public void setOrigen(Date origen) {
		this.origen = origen;
	}
	public boolean isInternacional() {
		return internacional;
	}
	public void setInternacional(boolean internacional) {
		this.internacional = internacional;
	}
	
	
}
