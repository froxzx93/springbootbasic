package com.example.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

import com.example.dto.DTOClienteInput;

@Configuration
public class RedisConfiguration {

	@Bean(name = "factory")
	public JedisConnectionFactory jedisConnectionFactory() {
		return new JedisConnectionFactory();
	}

	@Bean(name = "common")
    @Autowired
	public RedisTemplate<String, DTOClienteInput> redisTemplate() {
		final RedisTemplate<String, DTOClienteInput> redisTemplate = new RedisTemplate<>();
		redisTemplate.setConnectionFactory(jedisConnectionFactory());
		return redisTemplate;
	}

}
